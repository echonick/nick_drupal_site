<?php

/**
 * @file
 * template.php
 */

function nick_drupal_form_comment_form_alter(&$form, &$form_state, &$form_id) {
  $form['comment_body']['#after_build'][] = 'nick_drupal_customize_comment_form';  
}

function nick_drupal_customize_comment_form(&$form) {  
  $form[LANGUAGE_NONE][0]['format']['#access'] = FALSE; // Note LANGUAGE_NONE, you may need to set your comment form language code instead    
  return $form;  
}